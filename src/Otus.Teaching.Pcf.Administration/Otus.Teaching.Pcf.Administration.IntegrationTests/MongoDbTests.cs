﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Xunit.Abstractions;
using MongoDB.Bson;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests
{
    public class MongoDbTests
    {
        private CancellationTokenSource cancel;        
        private IMongoClient mongoClient;


        [Fact]
        public async Task t1()
        {
            mongoClient = new MongoClient("mongodb://mongoadmin:_Test123@localhost:28017");
            cancel = new CancellationTokenSource(10000);

            var db = mongoClient.GetDatabase("test");


            var coll = db.GetCollection<IntestA>("IndexTest");

            await coll.InsertOneAsync(new IntestA
            {
                Name = "drt67gdr6yd"               
            });

            var res = await coll.FindAsync(a => a.Name == "drt67gdr6yd");
            bool b = await res.MoveNextAsync();
        }

        private class IntestA
        {
            public ObjectId Id { get; set; }
            public string Name { get; set; }
         
        }
    }

   
}

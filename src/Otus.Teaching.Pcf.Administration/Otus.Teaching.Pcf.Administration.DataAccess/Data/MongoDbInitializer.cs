﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoClient mongoClient;
        private readonly IConfiguration configuration;

        public MongoDbInitializer(IMongoClient mongoClient, IConfiguration configuration)
        {
            this.mongoClient = mongoClient;
            this.configuration = configuration;
        }
        public void InitializeDb()
        {
            mongoClient.DropDatabase(configuration["MongoDb:Database"]);
            var database = mongoClient.GetDatabase(configuration["MongoDb:Database"]);

            var roleCollection = database.GetCollection<Role>("roles");
            roleCollection.InsertMany(FakeDataFactory.Roles);

            var employeeCollection = database.GetCollection<Employee>("employees");
            employeeCollection.InsertMany(FakeDataFactory.Employees);
            /*foreach (var employee in FakeDataFactory.Employees)
            {
               
            }*/
        }
    }
}